import { LitElement } from 'lit-element';

declare class LitPage extends LitElement {
  path: String
  hash: String
  query: String
  queryObject: {
    [key: string]: any
  }
  paramObject: {
    [key: string]: any
  }
}

export { LitPage }
