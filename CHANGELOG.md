# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.4](https://gitlab.com/tjmonsi/lit-page/compare/v0.0.3...v0.0.4) (2020-08-30)


### Bug Fixes

* fix typings ([c7c8144](https://gitlab.com/tjmonsi/lit-page/commit/c7c8144c0cfc25c9fe6439cb4bd2e86831a3bd13))

### [0.0.3](https://gitlab.com/tjmonsi/lit-page/compare/v0.0.2...v0.0.3) (2020-08-30)


### Bug Fixes

* add typings ([3ec382b](https://gitlab.com/tjmonsi/lit-page/commit/3ec382b5472b37c4ed5cc542e1e3d5c895e51394))

### [0.0.2](https://gitlab.com/tjmonsi/lit-page/compare/v0.0.1...v0.0.2) (2020-08-30)


### Bug Fixes

* add typings ([699a547](https://gitlab.com/tjmonsi/lit-page/commit/699a54766aa969810f3012932c25893a7c1c423b))

### 0.0.1 (2020-08-30)


### Features

* add properties ([3fe4054](https://gitlab.com/tjmonsi/lit-page/commit/3fe40545cba0d9f16df615fed6fe74e75527e1ad))
* add properties ([a79a37a](https://gitlab.com/tjmonsi/lit-page/commit/a79a37aac979e8c9fb0e64008816e8cf667a4629))
