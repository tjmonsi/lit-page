```js script
import { html } from '@open-wc/demoing-storybook';
import '../lit-page.js';

export default {
  title: 'LitPage',
  component: 'lit-page',
  options: { selectedPanel: "storybookjs/knobs/panel" },
};
```

# LitPage

A component for...

## Features:

- a
- b
- ...

## How to use

### Installation

```bash
yarn add lit-page
```

```js
import 'lit-page/lit-page.js';
```

```js preview-story
export const Simple = () => html`
  <lit-page></lit-page>
`;
```

## Variations

###### Custom Title

```js preview-story
export const CustomTitle = () => html`
  <lit-page title="Hello World"></lit-page>
`;
```
